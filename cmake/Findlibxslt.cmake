
FIND_PATH(xslt_INCLUDE_DIR xslt.h
  ${CMAKE_INSTALL_PREFIX}/include/libxslt
)

SET(xslt_NAMES ${xslt_NAMES} xslt)
FIND_LIBRARY(xslt_LIBRARY
  NAMES ${xslt_NAMES}
  HINTS
  ${CMAKE_INSTALL_PREFIX}/lib
  PATHS
  ${CMAKE_INSTALL_PREFIX}/lib
  )

SET(xslt_NAMES ${xslt_NAMES} xslt-config)
FIND_FILE(xslt_CONFIG
NAMES ${xslt_NAMES}
HINTS
  ${CMAKE_INSTALL_PREFIX}/bin
PATHS
  ${CMAKE_INSTALL_PREFIX}/bin
)
  
IF (xslt_LIBRARY AND xslt_INCLUDE_DIR)
    SET(xslt_LIBRARIES ${xslt_LIBRARY})
    SET(xslt_FOUND "YES")
ELSE (xslt_LIBRARY AND xslt_INCLUDE_DIR)
  SET(xslt_FOUND "NO")
ENDIF (xslt_LIBRARY AND xslt_INCLUDE_DIR)


IF (xslt_FOUND)
   IF (NOT xslt_FIND_QUIETLY)
      MESSAGE(STATUS "Found xslt headers: ${xslt_INCLUDE_DIR}")
      MESSAGE(STATUS "Found xslt library: ${xslt_LIBRARIES}")
   ENDIF (NOT xslt_FIND_QUIETLY)
ELSE (xslt_FOUND)
   IF (xslt_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find xslt library")
   ENDIF (xslt_FIND_REQUIRED)
ENDIF (xslt_FOUND)

# Deprecated declarations.
SET (NATIVE_xslt_INCLUDE_PATH ${xslt_INCLUDE_DIR} )
GET_FILENAME_COMPONENT (NATIVE_xslt_LIB_PATH ${xslt_LIBRARY} PATH)

MARK_AS_ADVANCED(
  xslt_LIBRARY
  xslt_INCLUDE_DIR
  xslt_CONFIG
  )
